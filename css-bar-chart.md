## CSS bar chart

- HTML

```
<div id="wrapper"> 
</div>
```

- CSS

```
#wrapper {
  width: var(--width);
  height: var(--height);
  border: 1px solid #ccc;
}

#wrapper > div {
  background: steelblue;
  height: var(--div-height);
  margin-bottom: var(--pad);
}

#wrapper > div:last-of-type {
  margin-bottom: 0;
}
```

- JS

```
console.clear()

const data = [10,20,50,100,30, 10]

const [dmin, dmax] = [Math.min(...data), Math.max(...data)]

const width = 400
const height = 300
const pad = 5

const wrElem = document.querySelector('#wrapper')

wrElem.style.setProperty('--width', `${width}px`)
wrElem.style.setProperty('--height', `${height}px`)

const scale = (minVal, maxVal, minPx, maxPx) => {
  const m = (maxPx - minPx)/(maxVal - minVal)
  const b = minPx - m * minVal
  return val => m * val + b
}

const bands = (cnt, pad, minPx, maxPx) => {
  const d = (maxPx - minPx - (cnt - 1) * pad) / cnt
  const m = (maxPx - minPx - d) / (cnt - 1)
  const b = minPx + d / 2 - m
  return {
    toPx (idx) { return m * idx + b },
    bandwidth () { return d}
  }
}


const y = bands(data.length, pad, 0, height)
const x = scale(dmin-10, dmax, 0, width)

wrElem.style.setProperty('--div-height', `${y.bandwidth()}px`)
wrElem.style.setProperty('--pad', `${pad}px`)

const h = data.map(d => {
  return `<div style="width: ${x(d)}px;"></div>`
}).join("\n")
wrElem.innerHTML = h
```

