## D3v5 stock chart

- HTML

```
<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/5.8.0/d3.js"></script>

<svg />
```

- CSS

```
line {
  fill: none;
  stroke: steelblue;
  stroke-width: 2px;
}

dot {
  fill: darkred;
}
```

- JS

```
console.clear()

const json = '[{"date":"2019-01-03","close":97.4},{"date":"2019-01-04","close":101.93},{"date":"2019-01-07","close":102.06},{"date":"2019-01-08","close":102.8},{"date":"2019-01-09","close":104.27},{"date":"2019-01-10","close":103.6},{"date":"2019-01-11","close":102.8},{"date":"2019-01-14","close":102.05},{"date":"2019-01-15","close":105.01},{"date":"2019-01-16","close":105.38},{"date":"2019-01-17","close":106.12},{"date":"2019-01-18","close":107.71},{"date":"2019-01-22","close":105.68},{"date":"2019-01-23","close":106.71},{"date":"2019-01-24","close":106.2},{"date":"2019-01-25","close":107.17},{"date":"2019-01-28","close":105.08},{"date":"2019-01-29","close":10294},{"date":"2019-01-30","close":106.38},{"date":"2019-01-31","close":104.43},{"date":"2019-02-01","close":102.78}]'

const data = JSON.parse(json)

const width = 300
const height = 400
const margin = 80

const drawBar = false

const x = d3.scaleBand()
  .domain(data.map(d => d.date))
  .range([0, width])
  .padding(0.1)

const dmin = d3.min(data.map(d => d.close))

const y = d3.scaleLinear()
  .domain([dmin - 5, d3.max(data.map(d => d.close))])
  .range([height, 0])

const line = d3.line()
  .x(d => x(d.date) + x.bandwidth()/2)
  .y(d => y(d.close))

const svg = d3.select('svg')
  .attr('width', width + 2 * margin)
  .attr('height', height + 2 * margin)

const g = svg
  .append('g')
  .attr('transform', `translate(${margin},${margin})`)

g.append('g')
  .attr('transform', `translate(0, ${height})`)
  .call(d3.axisBottom(x))
  .selectAll('text')
  .attr('y', 0)
  .attr('x', 9)
  .attr('dy', '.35em')
  .attr('transform', 'rotate(90)')
  .style('text-anchor', 'start');

g.append('g')
  .call(d3.axisLeft(y))

if (drawBar) {
  g.selectAll('.bar')
      .data(data)
      .enter().append('rect')
      .attr('fill', 'darkgrey')
      .attr('x', (d, i) => x(d.date))
      .attr('y', d => y(d.close))
      .attr('width', x.bandwidth())
      .attr('height', d => height - y(d.close))

  // alternative to attr('fill', ...)
  //     .attr('class', 'bar') 
  // and use CSS
  // 	.bar {
  // 	  fill: steelblue;
  // 	}  
}

g.append('path')
  .data([data])
  .attr('class', 'line')
  .attr('d', line)

g.selectAll('dot')
  .data(data)
  .enter().append('circle')
  .attr('class', 'dot')
  .attr('r', 3)
  .attr('cx', d => x(d.date) + x.bandwidth()/2)
  .attr('cy', d => y(d.close))

```