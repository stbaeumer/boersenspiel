const backendUrl = 'https://dbgarden.pw/websql'
//

const credentials = {
    user: "dbuser01",
    password: "_secret_!!"
}

// populate with dom elements when the document is loaed
const buttons = {}
currentJSON = null
let token = null

const login = async (user, password) => {
    const formData = new FormData()
    formData.append('user', user)
    formData.append('password', password)

    const url = `${backendUrl}/api/auth`
    let token = null
    try {
        const resp = await fetch(url, {method: 'POST', body: formData})
        if (resp.ok) {
            const json = await resp.json()
            token = json.token
        }
    } catch (err) {
        console.log(err)
    }
    return token
}

// sets global variable token
const getToken = async () => {
    token = await login(credentials.user, credentials.password)
    if (token) {
        buttons.makeInit.disabled = false
    }
}

// prepare sql statements
//const DropTable =`DROP TABLE Aktie1`
const CreateAktie = `CREATE TABLE IF NOT EXISTS Aktie1 (
                        symbol VARCHAR(12),
                        aktie VARCHAR(50),
                        stoplossBetrag INT,
                        PRIMARY KEY (symbol)
                    );`
                   
const InsertAktie = (psymbol, pAktie,pStoplossBetrag) => {
    return `INSERT into Aktie1 (symbol, aktie, stoplossBetrag) VALUES(
    '${psymbol}', '${pAktie}', '${pStoplossBetrag}');`
    }
console.log(InsertAktie('DE000BASF111', 'BASF',''))
const executeQueries = async () => {
    const queries = [CreateAktie,
                    InsertAktie('DE0008404005', 'Allianz Aktie',''),
                    InsertAktie('DE000BASF111', 'BASF',''),
                    ]
    let queryNr = 0

    // create database and fill some Data
    for (let i=0; i < queries.length; i++) {
        
        const query = queries[i]
        // query send to database
        const json = await dbCmd(query) 
    }
}

const clearResult = () => {
    document.querySelector('#result').innerHTML = ''
}

// Send a SQL Query to the MySql-Database
const dbCmd = async query => {
    var formData = new FormData()
    formData.append('query', query)
    var url = `${backendUrl}/api/query`
    // config for fetch
    // uses global token
    var config = {
        method: 'POST',
        headers: {
            'Authorization': `Bearer ${token}`
        },
        body: formData
    }

    try {
        var resp = await fetch(url, config)
        if (resp.ok) {
            var json = await resp.json()
            console.log(json)
            return json
        }
    } catch (err) {
        console.log(err)
    }
}

const getAktien = async () => {
    data =await dbCmd('SELECT * FROM Aktie1')
    console.log(data)
    const liItems = data.rows
        .map(row => `<li>${row.aktie}</li>`)
        .join('')
    document.querySelector('#result').innerHTML = `<ul>${liItems}</ul>`
    buttons.clearResult.disabled = false
}

const insAktien = async () => {
    if (document.readyState === 'complete') {
        let symbol = document.getElementById("idsymbol").value
        let stoplossBetrag = document.getElementById("stoplossBetrag").value
        let aktie = document.getElementById("idAktie").value
        data =await dbCmd(InsertAktie(symbol, aktie, stoplossBetrag))
        getAktien()
    }
}
const delAktienName = async () => {
    if (document.readyState === 'complete') {
        let aktie = document.getElementById("idAktie").value
        data = await dbCmd(DeleteAktie(aktie))
        getAktien()
    }
}
const DeleteAktie = (pSymbol) => {
    console.log("DELETE ONE")
    return `DELETE FROM Aktie1 WHERE symbol = '${pSymbol}';`
}


const getQuotes = async stock => {
    const json = await fetch(`https://api.iextrading.com/1.0/stock/${stock}/time-series`).then(r => r.json())
    const data = json.map(x => {              
       return {
        date: x.date,
        close: x.close
      } 
    })
    //Werte für das Zeichnen
    currentJSON = data
        

    const html = '<h1>' + stock + '</h1>' +  toHtml(data)
    if(document.readyState === 'complete'){
    document.querySelector('#wrapper').innerHTML = html 
    }
    // 28.03.
    showChart()
}
const toHtml = data => {
    return data
    .map( d => ` <div><div>${d.date}</div><div>${d.close}</div></div>`)
    .join("\n")

    console.log(data)


}

const getQuotes1 = async () => { 
    if (document.readyState === 'complete'){
    const symbol = document.getElementById("idsymbol").value
    getQuotes(symbol) 
    
    }
} 

const stopLoss = async () => {
    if (document.readyState === 'complete'){
    const stoploss = document.getElementById("stoplossBetrag").value
    const symbol = document.getElementById("idsymbol").value
    const json = await fetch(`https://api.iextrading.com/1.0/stock/${symbol}/time-series`).then(r => r.json())

    const schlusskurs = json[json.length-1].close

    if(schlusskurs <= stoploss){
        data = await dbCmd(DeleteAktie(symbol))
        document.querySelector('#stoplossDiv').innerHTML = `Die Aktie mit dem Symbol ${symbol} wurde verkauft` 
    }
    getAktien()
      
}
}

const showChart = async () => {
    let dates = currentJSON.map(item => item.date);
    let closes = currentJSON.map(item => item.close); 
    // myChart statt chartDiv
    var ctx = document.getElementById("myChart").getContext("2d");
    const myChart = new Chart(ctx, {
        type: 'line',
    data: {
        // 28.03. eckige Klammern entfernen
        labels: dates,
        datasets: [{
            label: 'Series 1', // Name the series
            // 28.03. eckige Klammern entfernen
            data: closes, // Specify the data values array
            fill: false,
            borderColor: '#2196f3', // Add custom color border (Line)
            backgroundColor: '#2196f3', // Add custom color background (Points and Fill)
            borderWidth: 1 // Specify bar border width
        }]},
    options: {
      responsive: true, // Instruct chart js to respond nicely.
      maintainAspectRatio: false, // Add to prevent default behaviour of full-width/height 

    
    }
    
})
    
    
console.log(data)
}
// app init

const init = () => {
    if(document.readyState === 'complete'){
    // buttons setup
    buttons.getToken = document.querySelector('#get-token')
    buttons.makeInit = document.querySelector('#make-init')
    buttons.clearResult = document.querySelector('#clear-result')
    buttons.getAktien = document.querySelector('#get-aktien')
    buttons.insAktien = document.querySelector('#ins-aktien')
    buttons.deleteAktien = document.querySelector('#delete-aktien')
    buttons.getQuotes1 = document.querySelector('#get-quotes1')
    buttons.stopLoss = document.querySelector('#stopLoss')
    buttons.chart = document.querySelector('#chart')


    buttons.makeInit.disabled = true
    buttons.clearResult.disabled = true
    
    buttons.getToken.addEventListener('click', getToken)
    buttons.makeInit.addEventListener('click', executeQueries)
    buttons.clearResult.addEventListener('click', clearResult)
    buttons.getAktien.addEventListener('click', getAktien)
    buttons.insAktien.addEventListener('click', insAktien)
    buttons.deleteAktien.addEventListener('click', delAktienName)
    buttons.getQuotes1.addEventListener('click', getQuotes1)
    buttons.stopLoss.addEventListener('click', stopLoss)
    buttons.chart.addEventListener('click', showChart)
}
}

// init app when dom is loaded
document.onreadystatechange = () => {
    if (document.readyState === 'complete') {
        init()
    }
}
