-- DROP kann Sinn machen, wenn das Schema noch nicht endgültig angelegt ist

DROP TABLE depotposition;
DROP TABLE aktienkurs;
DROP TABLE aktie;
DROP TABLE depot;

CREATE TABLE IF NOT EXISTS depot  (
	idDepot INT AUTO_INCREMENT,
	spielername VARCHAR(30),
	startguthaben FLOAT,
	PRIMARY KEY(idDepot)
);

CREATE TABLE IF NOT EXISTS aktie  (  	
	isin VARCHAR(30),  	  	
  	firma VARCHAR(30),
  	PRIMARY KEY (isin)
);

CREATE TABLE IF NOT EXISTS aktienkurs (  	
	isin VARCHAR(30),  	  	
	zeitstempel TIMESTAMP,
	wert FLOAT,
	PRIMARY KEY (isin, zeitstempel),
	FOREIGN KEY (isin) REFERENCES aktie(isin)
);

CREATE TABLE IF NOT EXISTS depotposition (  	
	isin VARCHAR(30),  	  	
	zeitstempel TIMESTAMP,
	idDepot INT,
	anzahl INT,
	PRIMARY KEY (zeitstempel, isin, idDepot),
	FOREIGN KEY (idDepot) REFERENCES depot(idDepot),
	FOREIGN KEY (isin, zeitstempel) REFERENCES aktienkurs(isin, zeitstempel)
);

INSERT INTO depot (spielername, startguthaben) VALUES ("Klaus", 100);
INSERT INTO depot (spielername, startguthaben) VALUES ("Gabi", 100);
INSERT INTO depot (spielername, startguthaben) VALUES ("Pit", 1000);

INSERT INTO aktie (isin, firma) VALUES ("U1234", "Porsche");

INSERT INTO aktienkurs (isin, zeitstempel, wert) VALUES ("U1234", CURRENT_TIMESTAMP, 999);

INSERT INTO depotposition (isin, anzahl, zeitstempel, idDepot) VALUES ("U1234", 99, NOW(), 1);

SELECT * FROM depot;
SELECT * FROM depotposition;
SELECT * FROM aktie;
SELECT * FROM aktienkurs;

-- Alle Depotpositionen von Klaus:
SELECT depot.spielername, depotposition.zeitstempel FROM depot, depotposition WHERE depot.spielername = "Klaus";

-- Alle Depotpositionen von Klaus mit dem Wert des jeweiligen Aktienpakets beim Einkauf:
SELECT depot.spielername, depotposition.isin, depotposition.zeitstempel, aktienkurs.wert * depotposition.anzahl AS Einkaufswert 
FROM depot, depotposition, aktienkurs 
WHERE depot.spielername = "Klaus";
