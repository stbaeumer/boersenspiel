const backendUrl = 'https://dbgarden.pw/websql'
//

const credentials = {
    user: "dbuser01",
    password: "_secret_!!"
}

let currentJSON = null

console.clear()

const toHtml = data => {
  return data
    .map(d => `<div><div>${d.date}</div><div>$${d.close.toFixed(2)}</div></div>`)
    .join("\n")
}

const getQuotes = async stock => {

    const json = await fetch(`https://api.iextrading.com/1.0/stock/${stock}/time-series`).then(r => r.json())

    console.log(json)

    const data = json.map(x => {       
      return {
        date: x.date,
        close: x.close
      }
    })
    currentJSON = data


    const html = '<h1>' + stock + '</h1>' +  toHtml(data)

    document.querySelector('#wrapper').innerHTML = html
    
    getAktien()
}

// populate with dom elements when the document is loaed
const buttons = {}

let token = null

const login = async (user, password) => {
    const formData = new FormData()
    formData.append('user', user)
    formData.append('password', password)

    const url = `${backendUrl}/api/auth`
    let token = null
    try {
        const resp = await fetch(url, {method: 'POST', body: formData})
        if (resp.ok) {
            const json = await resp.json()
            token = json.token
        }
    } catch (err) {
        console.log(err)
    }
    return token
}

// sets global variable token
const getToken = async () => {
    token = await login(credentials.user, credentials.password)
    if (token) {
        buttons.makeInit.disabled = false
    }
}

// prepare sql statements
const DropTable =`DROP TABLE Aktie1`
const CreateAktie = `CREATE TABLE IF NOT EXISTS Aktie1 (
                        symbol VARCHAR(12),
                        aktie VARCHAR(50),
                        stoploss INT,
                        PRIMARY KEY (symbol)
                    );`
                   
const InsertAktie = (pSymbol, pAktie, pStopLoss) => {
    return `INSERT into Aktie1 (symbol, aktie, stoploss) VALUES(
    '${pSymbol}', '${pAktie}', '${pStopLoss}');`
}

// 29.11.

const DeleteAktie = (pSymbol) => {
    console.log("DELETE ONE")
    return `DELETE FROM Aktie1 WHERE symbol = '${pSymbol}';`
}

const UpdateAktie = (pSymbol, pAktie) => {    
    return `UPDATE Aktie1 SET aktie = '${pAktie}' WHERE symbol = '${pSymbol}'`
}

// 06.02.

const DeleteAktieAlle = () => {
    console.log("DELETE All")
    return `DELETE FROM Aktie1;`
}


// 14.3.
console.log(InsertAktie('FB', 'Facebook',0))

// 14.3.
const executeQueries = async () => {    
    const queries = [ 
                    // 7.3.       
                    //DropTable,
                    CreateAktie,
                    InsertAktie('FB', 'Facebook',0),
                    InsertAktie('MSFT', 'Microsoft',0),
                    InsertAktie('GM', 'General Motors',0),
                    ]
    let queryNr = 0

    // create database and fill some Data
    for (let i=0; i < queries.length; i++) {
        
        const query = queries[i]
        // query send to database
        const json = await dbCmd(query) 
    }
}

const clearResult = () => {
    document.querySelector('#result').innerHTML = ''
}

// Send a SQL Query to the MySql-Database
const dbCmd = async query => {
    var formData = new FormData()
    formData.append('query', query)
    var url = `${backendUrl}/api/query`
    // config for fetch
    // uses global token
    var config = {
        method: 'POST',
        headers: {
            'Authorization': `Bearer ${token}`
        },
        body: formData
    }

    try {
        var resp = await fetch(url, config)
        if (resp.ok) {
            var json = await resp.json()
            console.log(json)
            return json
        }
    } catch (err) {
        console.log(err)
    }
}

const getAktien = async () => {
    data = await dbCmd('SELECT * FROM Aktie1')
    console.log(data)
    const liItems = data.rows
        .map(row => `<li>${row.aktie}</li>`)
        .join('')
    document.querySelector('#result').innerHTML = `<ul>${liItems}</ul>`
    buttons.clearResult.disabled = false
}

const insAktien = async () => {
    if (document.readyState === 'complete') {
        let symbol = document.getElementById("idSymbol").value.toUpperCase()
        let aktie = document.getElementById("idAktie").value
        let stopLossBetrag = document.getElementById("stopLossBetrag").value
        data = await dbCmd(InsertAktie(symbol, aktie, stopLossBetrag))
        getQuotes(symbol)
        getAktien()
    }
}

// 29.11.

const delAktien = async () => {
    if (document.readyState === 'complete') {
        let symbol = document.getElementById("idSymbol").value
        data = await dbCmd(DeleteAktie(symbol))
        getAktien()
    }
}

// 06.02.

const delAktienAlle = async () => {
    if (document.readyState === 'complete') {        
        data = await dbCmd(DeleteAktieAlle())
        getAktien()
    }
}

const getKurse = async () => {
    if (document.readyState === 'complete') {
        let symbol = document.getElementById("idSymbol").value        
        getQuotes(symbol)                
    }
}

// 06.02.

const delAktienName = async () => {
    if (document.readyState === 'complete') {
        let aktie = document.getElementById("idAktie").value
        data = await dbCmd(DeleteAktie(aktie))
        getAktien()
    }
}

// 06.02.

const updateAktieNameById = async () => {
    if (document.readyState === 'complete') {
        let symbol = document.getElementById("idSymbol").value
        let aktie = document.getElementById("idAktie").value
        data = await dbCmd(UpdateAktie(symbol, aktie))
        getAktien()
    }
}

// 14.3. Zeichnen
const drawAktien = async () => {
    if(currentJSON !== null){
        let dates = currentJSON.map(item => item.date);
        let closes = currentJSON.map(item => item.close);
        var ctx = document.getElementById("myChart").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: dates,
                datasets: [{
                    label: 'Kursverlauf', // Name the series
                    data: closes, // Specify the data values array
                    fill: false,
                    borderColor: '#2196f3', // Add custom color border (Line)
                    backgroundColor: '#2196f3', // Add custom color background (Points and Fill)
                    borderWidth: 1 // Specify bar border width
                }]},
            options: {
            responsive: true, // Instruct chart js to respond nicely.
            maintainAspectRatio: false, // Add to prevent default behaviour of full-width/height 
            }
        })
    }
};






// 7.3.


const stopLoss = async () => {
    if (document.readyState === 'complete') {

        data = await dbCmd('SELECT * FROM Aktie1;')
        
        let stopLossOrders = "Ausgeführte Stop-Loss-Orders:"

        for (var i = 0; i < data.rows.length; i++) {

            let symbol = data.rows[i].symbol
            let stopLossBetrag = data.rows[i].stoploss

            const json = await fetch(`https://api.iextrading.com/1.0/stock/${symbol}/time-series`).then(r => r.json())

            let letzterSchlusskurs = json[json.length - 1].close

            console.log(symbol + " " + data.rows[i].aktie + " " + letzterSchlusskurs + "<" + stopLossBetrag)

            if(letzterSchlusskurs < stopLossBetrag)
            {
                data = await dbCmd(DeleteAktie(symbol))
                stopLossOrders += symbol + ", "
            }
        }

        document.querySelector('#stopLossDiv').innerHTML = stopLossOrders.replace(/,\s*$/, "")

        getAktien()
    }
}

// app init
const init = () => {
    // buttons setup
    buttons.getToken = document.querySelector('#get-token')
    buttons.makeInit = document.querySelector('#make-init')
    buttons.clearResult = document.querySelector('#clear-result')
    buttons.getAktien = document.querySelector('#get-aktien')
    buttons.insAktien = document.querySelector('#ins-aktien')
    // 29.11.
    buttons.delAktien = document.querySelector('#del-aktien')


    // 7.3.
    buttons.stopLoss = document.querySelector('#stopLoss')
    //14.3.
    buttons.drawAktien = document.querySelector('#drawAktien')


    // 06.02.
    buttons.delAktienAlle = document.querySelector('#del-aktien-alle')
    buttons.delAktienName = document.querySelector('#del-aktien-Name')
    buttons.updateAktieNameById = document.querySelector('#update-aktien-Name')
    buttons.getKurse = document.querySelector('#get-quotes')

    buttons.makeInit.disabled = true
    buttons.clearResult.disabled = true
    
    buttons.getToken.addEventListener('click', getToken)
    buttons.makeInit.addEventListener('click', executeQueries)
    buttons.clearResult.addEventListener('click', clearResult)
    buttons.getAktien.addEventListener('click', getAktien)
    buttons.insAktien.addEventListener('click', insAktien)
    // 29.11.
    buttons.delAktien.addEventListener('click', delAktien)
    // 06.02.


    // 14.3.
    buttons.drawAktien.addEventListener('click', drawAktien)
    // 7.3.
    buttons.stopLoss.addEventListener('click', stopLoss)


    buttons.delAktienAlle.addEventListener('click', delAktienAlle)
    buttons.delAktienName.addEventListener('click', delAktienName)
    buttons.updateAktieNameById.addEventListener('click', updateAktieNameById)
    buttons.getKurse.addEventListener('click', getKurse)
}

// init app when dom is loaded
document.onreadystatechange = () => {
    if (document.readyState === 'complete') {
        init()
    }
}
