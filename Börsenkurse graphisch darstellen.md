### Börsenkurse graphisch darstellen

1. Schritt: Wie erstellt man xy-Diagramme mitHilfe von der Javascript-Bibliothek chartjs?

   * Einbinden der Bibliothek in der HTML-Datei undex.html

     ```html
       <script src ="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.min.js"></script>     
     ```

     

   * Erstellen eines Buttons und einer Click-Funktion zum zeichnen des Graphen

     ```html
                     <!--14.3.-->
                     <button id="drawAktien">Zeichne den Aktienverlauf</button>
     
     ```

     

   * Das Chart wird in eine CANVAS-Zeichenfläche eingebettet. 

     ```html
      <div id="drawarea">
                     <canvas id="myChart" height = "400 px"></canvas>
                     </div>
     ```

     

2. Schritt: Das div wird analog zum Wrapper-div formatiert (css) nur die Breite für diesen Teil wird erhöht.

   * Breite: wrapper für die Anzeige der Daten 20%, Breite Chartbereich: 50%

     ```css
     #wrapper, #drawarea {
         display: flex;
         flex-direction: column;
         margin: 0 auto;
         border: 1px solid #aaa;
         border-radius: 3px;
         box-shadow: 3px 3px 5px #aaa;
       }
     
       #wrapper {
         width: 20%;
       }
       #drawarea {
         width: 50%;
       }
     ```

     

3. Schritt: Für das Zeichnen des Charts können die mit getquotes geholten Daten genutzt werden. Vereinfachend wird hierzu eine globale Variable im app.js genutzt. Die dann in der Funktion getquotes neu zugewiesen wird. 

   ```js
   //  aktuell in Zeile 9
   let currentJSON = null
   //
   // Erweiterung der getquotes-function in Zeile 18!
   //
   const getQuotes = async stock => {
   
       const json = await fetch(`https://api.iextrading.com/1.0/stock/${stock}/time-series`).then(r => r.json())
   
       console.log(json)
   
       const data = json.map(x => {       
         return {
           date: x.date,
           close: x.close
         }
       })
       currentJSON = data
   
   
       const html = '<h1>' + stock + '</h1>' +  toHtml(data)
   
       document.querySelector('#wrapper').innerHTML = html
       
       getAktien()
   }
   ```

   4.Schritt: Nun fehlt noch das gewöhnlich einbinden des Buttons und das programmieren der zugehörigen Zeichnenroutine:

   * **Zeile 4 und 5 sowie die Zeilen 10 , 12 und 13 habe ich ert nach dem Unterricht ergänzt, vorher hatten wir feste Werte verdrahtet es wurde immer dieselbe Chart angezeigt!**

   ```js
   // 14.3. Zeichnen
   const drawAktien = async () => {
       console.log(currentJSON);
       let dates = currentJSON.map(item => item.date);
       let closes = currentJSON.map(item => item.close);
       var ctx = document.getElementById("myChart").getContext('2d');
       var myChart = new Chart(ctx, {
           type: 'line',
           data: {
               labels: dates,
               datasets: [{
                   label: 'Kursverlauf', // Name the series
                   data: closes, // Specify the data values array
                   fill: false,
                   borderColor: '#2196f3', // Add custom color border (Line)
                   backgroundColor: '#2196f3', // Add custom color background (Points and Fill)
                   borderWidth: 1 // Specify bar border width
               }]},
           options: {
           responsive: true, // Instruct chart js to respond nicely.
           maintainAspectRatio: false, // Add to prevent default behaviour of full-width/height 
           }
       });
   }
   
   ```

   

5. Schritt einbinden: Das hat ungeführ ein Dritte lin der letzten Stunde bewerkstelligt!

   ```js
    // 14.3., s. Zeile 328/329
       buttons.drawAktien.addEventListener('click', drawAktien)
    //14.3., s. Zeile 305/306
       buttons.drawAktien = document.querySelector('#drawAktien')
   ```

   

Das Ergebnis sieht nun wie folgt aus:

![](C:\Users\gk\Documents\Offline\Schule 17\Programmierung\BörseMitKursverlauf\screenshotAnwendung.jpg)

### Ausbauideen

1. Das Zeichnen wäre auch interessant für mehrere Unternehmen gleichzeitig 
2. Langsam wird es Zeit die abgeholten Kurse auch i nder Datenbank abzulegen

