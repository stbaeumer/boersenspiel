# Stock Quotes

- HTML

```
<div id="wrapper">
</div>
```

- CSS

```
html {
  font-family: Helvetica;
  color: #333;
}

#wrapper {
  display: flex;
  flex-direction: column;
  margin: 0 auto;
  width: 400px;
  border: 1px solid #aaa;
  border-radius: 3px;
  box-shadow: 3px 3px 5px #aaa;
}

#wrapper > div {
  display: flex;
  justify-content: space-around;
  border-bottom: 1px solid #888;
}

#wrapper > div div {
  min-height: 50px;
  width: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0 5px;
  text-align: center;
}

#wrapper > div:nth-of-type(2n) {
  background: rgba(0,0,0,.2);
}

#wrapper > div:nth-of-type(2n+1) {
  background: rgba(0,0,0,.3);
}
```

- JS

```
console.clear()

const toHtml = data => {
  return data
    .map(d => `<div><div>${d.date}</div><div>$${d.close.toFixed(2)}</div></div>`)
    .join("\n")
}


const getQuotes = async stock => {
  const json = await fetch(`https://api.iextrading.com/1.0/stock/${stock}/time-series`).then(r => r.json())
  const data = json.map(x => {
    return {
      date: x.date,
      close: x.close
    }
  })
  // console.log(JSON.stringify(data))
  const html = toHtml(data)
  document.querySelector('#wrapper').innerHTML = html
}

getQuotes('msft')
```